﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$installArgs = '/S /D=' + $toolsDir

$packageArgs = @{
  packageName    = $env:ChocolateyPackageName
  url            = 'https://dist.torproject.org/torbrowser/12.0a1/torbrowser-install-12.0a1_en-US.exe'
  url64bit       = 'https://dist.torproject.org/torbrowser/12.0a1/torbrowser-install-win64-12.0a1_en-US.exe'
  checksum       = '55053cab1a8e69bc04ff1d897fc844d432e2c0e1018cbcd8d7baa639d3a66b1e'
  checksum64     = '2df9ac3430d01121d035a0029cf57d932bb386107d7809f60c4f11cf2e93f20f'
  checksumType   = 'sha256'
  checksumType64 = 'sha256'
  silentArgs     = $installArgs
}

Install-ChocolateyPackage @packageArgs

$files = Get-ChildItem $toolsDir -include *.exe -recurse
foreach ($file in $files) {
    New-Item "$file.ignore" -type file -force | Out-Null
}
