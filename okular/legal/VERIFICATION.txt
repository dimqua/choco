VERIFICATION
Verification is intended to assist the Chocolatey moderators and community
in verifying that this package's contents are trustworthy.

The installer have been downloaded from the Binary Factory for KDE and can be verified like this:

1. Download the following builds.

x86_64: https://binary-factory.kde.org/view/Windows%2064-bit/job/Okular_Nightly_win64/391/artifact/okular-master-391-windows-msvc2017_64-cl.exe

2. You can use one of the following methods to obtain the checksum(s):
  - use powershell function 'Get-Filehash',
  - use chocolatey utility 'checksum.exe'.

  checksum type: sha256
  checksum64: E4A96FFD60ED8999EEB6FD9ADDFEDC0609125CC7BC16DE885B7987B0A94E1FF6

The included 'LICENSE.txt' file have been obtained from: https://cgit.kde.org/okular.git/plain/COPYING.LIB?h=v20.04.1
