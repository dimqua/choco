import-module au

$releases = 'https://www.softpedia.com/get/Antivirus/Immunet-Protect.shtml'

function global:au_GetLatest {
    $downloads = Invoke-WebRequest -Uri $releases -UseBasicParsing
    $title = ($downloads.links | ? title -match 'Build' | select -Last 1 -expand title) -split ' '
    $version = ($title | select -First 1 -Skip 1) + '.' + ($title | select -First 1 -Skip 3)

    @{
        URL32   = 'https://download.immunet.com/binaries/immunet/bin/ImmunetSetup.exe'
        Version = $version
        ReleaseNotes = 'http://support.immunet.com/index.php?/forum/6-announcements/'

    }
}

function global:au_SearchReplace {
    @{
        "tools\chocolateyInstall.ps1" = @{
            "(?i)(^\s*url\s*=\s*)('.*')"      = "`$1'$($Latest.URL32)'" 
            "(?i)(^\s*checksum\s*=\s*)('.*')" = "`$1'$($Latest.Checksum32)'"
        }
        "immunet.nuspec" = @{
            "(\<releaseNotes\>).*(\<\/releaseNotes\>)" = "`${1}$($Latest.ReleaseNotes)`$2"
        }
    }
}

update
