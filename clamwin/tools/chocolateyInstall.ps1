﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName    = $env:ChocolateyPackageName
  url            = 'http://downloads.sourceforge.net/clamwin/clamwin-0.103.2.1-setup.exe'
  checksum       = 'ac9d76d87904efc62d976f7c6f8f1d9f459e9ad13cbe3e4e886005d3f14e0393'
  checksumType   = 'sha256'
  silentArgs     = '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-'
}

Install-ChocolateyPackage @packageArgs

Get-ChildItem $toolsDir\*.exe | ForEach-Object { Remove-Item $_ -ea 0; if (Test-Path $_) { Set-Content "$_.ignore" } }
