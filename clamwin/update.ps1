import-module au

$releases = 'http://www.clamwin.com/content/view/18/46/'

function global:au_GetLatest {
    $downloads = Invoke-WebRequest -Uri $releases -UseBasicParsing
    $url = $downloads.links | ? href -match '.exe$' | select -First 1 -expand href
    $version = $url -split '/' -split '-' | select -Last 1 -Skip 1

    @{
        URL32   = $url
        Version = $version
    }
}

function global:au_SearchReplace {
    @{
        "tools\chocolateyInstall.ps1" = @{
            "(?i)(^\s*url\s*=\s*)('.*')"      = "`$1'$($Latest.URL32)'" 
            "(?i)(^\s*checksum\s*=\s*)('.*')" = "`$1'$($Latest.Checksum32)'"
        }
    }
}

update
