﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
    packageName    = $env:ChocolateyPackageName
    fileType       = 'msi'
    url            = 'https://downloads.iridiumbrowser.de/windows/2022.04.100/iridiumbrowser-2022.04.100.0-x86.msi'
    url64bit       = 'https://downloads.iridiumbrowser.de/windows/2022.04.100/iridiumbrowser-2022.04.100.0-x64.msi'
    checksum       = '055211d8aa5c160db82f7893820570aaff04d994f635f491b99416da04f514f9'
    checksum64     = '0fa1306f13e80f91cc2afb957d9415e0ebe5cd3464bfc4b6461814c583cc014f'
    checksumType   = 'sha256'
    checksumType64 = 'sha256'
    silentArgs     = "/qn /norestart"
}

Install-ChocolateyPackage @packageArgs

Get-ChildItem $toolsDir\*.msi | ForEach-Object { Remove-Item $_ -ea 0; if (Test-Path $_) { Set-Content "$_.ignore" } }
