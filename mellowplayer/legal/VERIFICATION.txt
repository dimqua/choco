VERIFICATION

Verification is intended to assist the Chocolatey moderators and community
in verifying that this package's contents are trustworthy.

Package can be verified like this:

1. Go to

   x86: https://bintray.com/colinduquesnoy/MellowPlayer/download_file?file_path=continuous/3.7.0/MellowPlayer_Setup.exe

   to download the installer.

2. You can use one of the following methods to obtain the SHA256 checksum:
   - Use powershell function 'Get-FileHash'
   - Use Chocolatey utility 'checksum.exe'

   checksum32: CEE6C7E10CF28E0C289C32C2D6737CB059269C3A1B3A1A901DFBDA8FE46F927F

The included 'LICENSE.txt' file have been obtained from: https://gitlab.com/ColinDuquesnoy/MellowPlayer/raw/master/LICENSE
