VERIFICATION

Verification is intended to assist the Chocolatey moderators and community
in verifying that this package's contents are trustworthy.

Package can be verified like this:

1. Go to

x86:
x86_64: https://ci.appveyor.com/api/buildjobs/42uxu3axdyjav5o8/artifacts/redshift-windows-x86_64.zip

to download the ZIP-file.

2. You can use one of the following methods to obtain the checksum:
   - Use powershell function 'Get-FileHash'
   - Use Chocolatey utility 'checksum.exe'

   checksum type: sha256
   checksum32:
   checksum64: 134E42C2C4F4205DD86657A036BDCE8F4B606A9AAC29438A1198E09DD7CC5D41

The included 'LICENSE.txt' file have been obtained from: https://raw.githubusercontent.com/jonls/redshift/v1.12/COPYING
