import-module au
    
$tags = 'https://github.com/jonls/redshift/tags'
$commits = 'https://github.com/jonls/redshift/commits/master'

function global:au_GetLatest {
    $page   = Invoke-WebRequest -Uri $tags -UseBasicParsing
    $version_s = (($page.links | ? href -match '.zip$' | select -First 1 -expand href) -split '/|.zip' | select -First 1 -Skip 4).Replace('v','')
    $stable = ((($page.links | ? href -match '/commit/' | select -First 1 -expand href)  -split '/' | select -Last 1).ToCharArray() | select -first 7) -join ""
    $commit = ((((Invoke-WebRequest -Uri $tags -UseBasicParsing).links | ? href -match '/commit/' | select -First 1 -expand href) -split '/' | select -Last 1).ToCharArray() | select -first 7) -join ""
    $url32 = 'https://ci.appveyor.com/api/projects/jonls/redshift/artifacts/redshift-windows-i686.zip?commit=' + $commit + '&job=Environment%3A+arch%3Di686'
    $url64 = 'https://ci.appveyor.com/api/projects/jonls/redshift/artifacts/redshift-windows-x86_64.zip?commit=' + $commit + '&job=Environment%3A+arch%3Dx86_64'
    $jobId32 = [System.Net.HttpWebRequest]::Create($url32).GetResponse().ResponseUri.AbsolutePath -split '/' |  select -Last 1 -Skip 1
    $jobId64 = [System.Net.HttpWebRequest]::Create($url64).GetResponse().ResponseUri.AbsolutePath -split '/' |  select -Last 1 -Skip 1
    
    if ($commit -eq $stable) { $version = $version_s } else { $version = $version_s + '-g' + $commit }

    @{
        URL32   = 'https://ci.appveyor.com/api/buildjobs/' + $jobId32 + '/artifacts/redshift-windows-i686.zip'
        URL64   = 'https://ci.appveyor.com/api/buildjobs/' + $jobId64 + '/artifacts/redshift-windows-x86_64.zip'
        Version = $version
        Copying = 'https://raw.githubusercontent.com/jonls/redshift/v' + $version + '/COPYING'
        CopyingUrl = 'https://github.com/jonls/redshift/blob/v' + $version + '/COPYING'
        ReleaseNotes = 'https://github.com/jonls/redshift/releases/tag/v' + $version_s
        Options = '@{Headers=@{ "Authorization" = "Bearer $token"}}'
    }
}

function global:au_BeforeUpdate {
    Invoke-WebRequest -Uri $Latest.Copying -OutFile "legal\LICENSE.txt"
    Get-RemoteFiles -Purge -NoSuffix
}

function global:au_SearchReplace {
   @{
    ".\tools\chocolateyInstall.ps1" = @{
        "(?i)(^\s*file\s*=\s*`"[$]toolsDir\\).*"   = "`${1}$($Latest.FileName32)`""
        "(?i)(^\s*file64\s*=\s*`"[$]toolsDir\\).*" = "`${1}$($Latest.FileName64)`""
        }
    ".\legal\VERIFICATION.txt" = @{
        "(?i)(x86:).*"        = "`${1} $($Latest.URL32)"
        "(?i)(x86_64:).*"     = "`${1} $($Latest.URL64)"
        "(?i)(checksum32:).*" = "`${1} $($Latest.Checksum32)"
        "(?i)(checksum64:).*" = "`${1} $($Latest.Checksum64)"
        "(?i)(The included 'LICENSE.txt' file have been obtained from:).*"  = "`${1} $($Latest.Copying)"
        }
    "redshift.nuspec" = @{
          "(\<licenseUrl\>).*(\<\/licenseUrl\>)" = "`${1}$($Latest.CopyingUrl)`$2"
          "(\<releaseNotes\>).*(\<\/releaseNotes\>)" = "`${1}$($Latest.ReleaseNotes)`$2"
        }
    }
}

update -ChecksumFor none
